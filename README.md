# RxBatteryTest

This is a proof of concept application that tries to read battery info from an Android phone using the [RxBattery library](https://github.com/pwittchen/RxBattery) and the [BatteryManager API](https://developer.android.com/training/monitoring-device-state/battery-monitoring.html).
