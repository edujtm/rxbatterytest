package com.example.rxbatterytest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.example.rxbatterytest.battery.RxBattery
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private val disposable = CompositeDisposable()
    private lateinit var voltTextView : TextView
    private lateinit var tempTextView : TextView
    private lateinit var currentAvgView : TextView
    private lateinit var currentNowView : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        voltTextView = findViewById(R.id.tv_voltage)
        tempTextView = findViewById(R.id.tv_temperature)
        currentAvgView = findViewById(R.id.tv_current_avg)
        currentNowView = findViewById(R.id.tv_current_now)
    }

    override fun onStart() {
        super.onStart()
        val listener = RxBattery
            .observe(this)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ batteryState ->
                // Voltagem
                voltTextView.text = getString(R.string.voltage_label, batteryState.voltage / 1000.0)
                // Temperatura em decimos de centigrado
                tempTextView.text = getString(R.string.temperature_label, batteryState.temperature / 10.0)
                currentAvgView.text = getString(R.string.current_avg_label, batteryState.currentAvg)
                currentNowView.text = getString(R.string.current_now_label, batteryState.currentNow)
            }, { error ->
                Log.d("RXBATTERY", error.message)
            })

        disposable.add(listener)
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}
