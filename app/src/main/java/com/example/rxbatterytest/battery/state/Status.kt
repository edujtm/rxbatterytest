package com.example.rxbatterytest.battery.state

enum class Status {
    CHARGING,
    DISCHARGING,
    FULL,
    NOT_CHARGING,
    UNKNOWN
}