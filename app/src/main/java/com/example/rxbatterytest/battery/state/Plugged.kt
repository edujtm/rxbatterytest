package com.example.rxbatterytest.battery.state

enum class Plugged {
    AC,
    USB,
    WIRELESS,
    UNKNOWN
}