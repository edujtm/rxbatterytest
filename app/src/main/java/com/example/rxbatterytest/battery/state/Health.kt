package com.example.rxbatterytest.battery.state

enum class Health {
    COLD,
    DEAD,
    GOOD,
    OVERHEAT,
    OVER_VOLTAGE,
    UNKNOWN,
    UNSPECIFIED_FAILURE
}